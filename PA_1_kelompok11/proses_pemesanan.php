<?php
session_start();

// Periksa apakah pengunjung sudah login atau belum
if (!isset($_SESSION['akun_id'])) {
    // Jika belum login, redirect ke halaman login
    header("Location: produk.php"); // Ganti "login.php" dengan halaman login yang sesuai
    exit();
}

require('config.php');

// Periksa koneksi
if (mysqli_connect_errno()) {
    echo "Gagal terhubung ke MySQL: " . mysqli_connect_error();
    exit();
}

// Ambil nama akun dari sesi yang sudah login
$user_name = $_SESSION['username'];

// Query untuk mengambil id_akun berdasarkan nama_akun
$query = "SELECT id_akun FROM akun WHERE username = '$user_name'";

// Eksekusi query
$result = mysqli_query($conn, $query);

if ($result && mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $id_akun = $row['id_akun'];

    // Query untuk menampilkan data dari tabel keranjang
    $query_select = "SELECT * FROM keranjang WHERE id_akun = '$id_akun' ORDER BY id_keranjang DESC";
    $result_select = mysqli_query($conn, $query_select);

    $query = "SELECT whatsapp_pesan FROM setting";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result);

    $whatsapp_pesan = $row['whatsapp_pesan'];

    if ($result_select && mysqli_num_rows($result_select) > 0) {
        // Mengirim pesan ke WhatsApp
        function sendWhatsAppMessage($produkList, $user_name, $whatsapp_pesan)
        {
            // Ganti pesan dengan pesan yang ingin dikirimkan
            $pesan = "*Pemesan Produk*: " . $user_name . "\n";
            
            foreach ($produkList as $produk) {
                $pesan .= "\n";
                $pesan .= "*Nama Produk*: " . $produk['nama_produk'] . "\n";
                $pesan .= "*Harga*: " . $produk['harga'] . "\n";
                $pesan .= "*Deskripsi*: " . $produk['deskripsi_produk'] . "\n";
                $pesan .= "*Jumlah*: " . $produk['jumlah'] . "\n";
                $pesan .= "*Total Harga*: " . $produk['total_harga'] . "\n";
                $pesan .= "*Lokasi*: " . $produk['lokasi'] . "\n";
                $pesan .= "*Pesan*: " . $produk['pesan'] . "\n";
            }

            // Format link untuk mengirim pesan WhatsApp
            $url = $whatsapp_pesan . "&text=" . urlencode($pesan);


            // Redirect ke link WhatsApp
            echo "<script>window.location.href = '" . $url . "';</script>";
            echo "<meta http-equiv='refresh' content='0; url=produk.php'>";
        }

        // Mengumpulkan semua produk dalam keranjang
        $produkList = [];
        while ($row_select = mysqli_fetch_assoc($result_select)) {
            $produkList[] = $row_select;
        }

        // Mengirim pesan WhatsApp dengan semua data produk
        sendWhatsAppMessage($produkList, $user_name, $whatsapp_pesan);
    } else {
        echo "Tidak ada data dalam keranjang.";
    }
}

// Tutup koneksi ke database
mysqli_close($conn);
