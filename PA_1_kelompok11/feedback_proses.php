    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Feedback</title>
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
        <link rel="stylesheet" href="css/footerr.css">
        <link rel="stylesheet" href="css/feedbac.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    </head>

    <body>



        <?php


        session_start();

        // Periksa apakah pengunjung sudah login atau belum
        if (!isset($_SESSION['akun_id'])) { ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'warning',
                    title: 'Login diperlukan',
                    text: 'Anda harus masuk terlebih dahulu sebelum membuat feedback !',
                    onClose: function() {
                        window.location.href = "login.php";
                    }
                });
            </script>
        <?php exit();
        } ?>
        <?php

        if (isset($_SESSION['username'])) {
            $username = $_SESSION['username'];
        } else {
            $username = 'Belum Login';
        }



        // memanggil file config.php
        require_once "config.php";

        // menangkap data yang dikirim dari form
        $name = $_POST["name"];
        $email = $_POST["email"];
        $feedback = $_POST["feedback"];

        // memasukkan data ke dalam tabel feedback
        $sql = "INSERT INTO feedback (nama, email, komentar) VALUES ('$name', '$email', '$feedback')";

        if (mysqli_query($conn, $sql)) {?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Feedback berhasil terkirim !',
                    onClose: function() {
                        window.location.href = "feedback.php";
                    }
                });
            </script>
        <?php exit();
        } 
         else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }

        // menutup koneksi
        mysqli_close($conn);
        ?>
    </body>

    </html>