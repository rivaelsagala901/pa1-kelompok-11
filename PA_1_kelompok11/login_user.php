<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login_user</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <style>
    .container {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }

    .card {
      max-width: 700px;
      width: 90%;
      border-radius: 45px;
    }

    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: 50vh;
    }
  </style>
</head>

<body class="header finisher-header">
  <div class="container" data-aos="flip-down">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <img src="uploads/logo sekka2.jpg " class="img-fluid" alt="..." width="200px">
          <br>
          <h1 class="text-center"><b>Login  Website</b></h1>
          <hr>
          <form action="login_process.php" method="post">
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">
                <h3>Username</h3>
              </label>
              <input type="text" class="form-control rounded-pill" id="exampleFormControlInput1" name="username" style="max-width: 300px;">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">
                <h3>Password</h3>
              </label>
              <input type="password" class="form-control rounded-pill" id="exampleFormControlInput1" name="password" style="max-width: 300px;">
            </div>
            <button class="btn btn-primary rounded-pill" name="login">Masuk</button>
            <span>Belum Memiliki Akun? | <a href="daftar.php" class="text-decoration-none">Daftar Sekarang</a></span>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <script src="js/finisher-header.es5.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    new FinisherHeader({
      "count": 10,
      "size": {
        "min": 2,
        "max": 40,
        "pulse": 0
      },
      "speed": {
        "x": {
          "min": 0,
          "max": 0.8
        },
        "y": {
          "min": 0,
          "max": 0.2
        }
      },
      "colors": {
        "background": "#15182e",
        "particles": [
          "#ff926b",
          "#87ddfe",
          "#acaaff",
          "#1bffc2",
          "#f9a5fe"
        ]
      },
      "blending": "screen",
      "opacity": {
        "center": 1,
        "edge": 1
      },
      "skew": 0,
      "shapes": [
        "c",
        "s",
        "t"
      ]
    });
  </script>
</body>

</html>