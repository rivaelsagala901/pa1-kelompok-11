<?php
require "config.php";

$username = mysqli_real_escape_string($conn, $_POST["username"]); // menghindari SQL injection
$password = $_POST["password"];

$query_sql = "SELECT * FROM akun WHERE username = '$username'";

$result = mysqli_query($conn, $query_sql);

if (mysqli_num_rows($result) > 0) {
    $row_akun = mysqli_fetch_array($result);
    $hashedPassword = $row_akun['password'];

    if (password_verify($password, $hashedPassword)) {
        // Password cocok
        $roll = $row_akun['roll'];
        session_start();
        $_SESSION["akun_id"] = $row_akun["id_akun"];
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $hashedPassword;
        if ($roll == 'admin') {
            header("Location: admind");
        } elseif ($roll == 'user') {
            header("Location: produk.php");
        }
        exit(); // keluar dari script setelah header() dijalankan
    } else {
        echo '<script>alert("Password salah!"); window.location.href = "login.php";</script>';
        exit(); // keluar dari script setelah alert() dijalankan
    }
} else {
    echo '<script>alert("Username tidak ditemukan!"); window.location.href = "login.php";</script>';
    exit(); // keluar dari script setelah alert() dijalankan
}
