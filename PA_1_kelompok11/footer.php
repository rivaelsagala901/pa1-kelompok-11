  <?php
    require "config.php";


    // Query untuk mengambil data produk yang akan diubah
    $query = "SELECT * FROM setting";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result);
    ?>


  <footer class="bg-dark text-light">
      <div class="container py-5">
          <div class="row">
              <div class="col-md-4" data-aos="fade-down" data-aos-delay="">
                  <h5 class="mb-4">Ikuti Kami</h5>
                  <ul class="list-inline">
                      <li class="list-inline-item">
                          <a href="<?php echo $row['facebook_toko'] ?>" target="_blank">
                              <img src="gambar/Vector (1).png" alt="Facebook" class="social-icon">
                          </a>
                      </li>
                      <li class="list-inline-item">
                          <a href="<?php echo $row['whatsapp'] ?>" target="_blank">
                              <img src="gambar/ri_whatsapp-fill (1).png" alt="Whatsapp" class="social-icon">
                          </a>
                      </li>
                      <li class="list-inline-item">
                          <a href="<?php echo $row['instagram_toko'] ?>" target="_blank">
                              <img src="gambar/uil_instagram-alt (1).png" alt="Instagram" class="social-icon">
                          </a>
                      </li>
                  </ul>
              </div>
              <div class="col-md-4" data-aos="fade-up " data-aos-delay="500" data-aos-duration="1000">
                  <p class="mb-4 text-center">&copy; 2023 Sekka Craft. Seluruh hak cipta dilindungi Undang-Undang.</p>
              </div>
              <div class="col-md-4">
                  <h6 class="mb-4 text-center">
                      Terima kasih telah <br> mengunjungi situs web kami!
                  </h6>
              </div>
          </div>
      </div>
  </footer>