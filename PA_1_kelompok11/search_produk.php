<?php
// Koneksi ke database
$servername = "localhost";
$username = "root";
$password = "";
$db_name = "sekka_craftt";
$port = "3306";
try {
    $dsn = "mysql:host=$servername;dbname=$db_name;charset=utf8mb4;port=$port";
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $username, $password, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

// Dapatkan kata kunci pencarian dari parameter GET
$keyword = $_GET['keyword'] ?? '';

// Persiapkan statement SQL untuk pencarian berdasarkan nama produk
$stmt = $pdo->prepare("SELECT * FROM produk WHERE nama_produk LIKE ?");
$stmt->execute(["%$keyword%"]);

// Dapatkan hasil pencarian dalam bentuk array asosiatif
$results = $stmt->fetchAll();

// Kembalikan hasil pencarian sebagai respons JSON
header('Content-Type: application/json');
echo json_encode($results);
?>



