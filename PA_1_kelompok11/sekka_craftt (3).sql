-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2023 at 06:29 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sekka_craftt`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `roll` varchar(12) NOT NULL DEFAULT 'user',
  `last_login` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `password`, `roll`, `last_login`) VALUES
(42, 'Rivael', '$2y$10$xQRYovQ9K.B7jIsDJVcaOOv9OCbA6khPkN2PPnHBWQUAV.8DDbrvu', 'admin', '2023-06-06 14:13:38'),
(44, 'sagala', '$2y$10$GRoVre3h2Q9q1P/HU9poD.V.BCCIyTynHoVeyLbsZLB7w2k6S3peW', 'user', '2023-06-06 14:15:25'),
(45, 'sambo', '$2y$10$jXk6vRnrOu9IxsRdX05Eg.iD7v5f5uaR.U1JkXHosp/A/NbTTSqvm', 'user', '2023-06-06 15:46:52'),
(46, 'jokowi', '$2y$10$FZpYDrYWF9Zcr10qZ7DSnO/sdGusZlEMvFcFsYaqdohEwVcBJYO1G', 'user', '2023-06-08 23:07:03');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id_feedback` int(11) NOT NULL,
  `nama` varchar(55) NOT NULL,
  `email` varchar(44) NOT NULL,
  `komentar` text NOT NULL,
  `waktu` datetime DEFAULT current_timestamp(),
  `aksi` varchar(45) NOT NULL DEFAULT 'tampil'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id_feedback`, `nama`, `email`, `komentar`, `waktu`, `aksi`) VALUES
(60, 'Rivael Sugianto Sagala', 'rivaelsagala901@gmail.com', 'sdfghjjjjjjjjjjjjjjjjjjjjjjj', '2023-06-17 23:13:34', 'tampil');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `id_akun` int(11) NOT NULL,
  `nama_produk` varchar(800) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `deskripsi_produk` varchar(900) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` varchar(100) NOT NULL,
  `lokasi` varchar(300) NOT NULL,
  `pesan` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `id_akun`, `nama_produk`, `harga`, `deskripsi_produk`, `jumlah`, `total_harga`, `lokasi`, `pesan`) VALUES
(56, 42, ' Topi  - Bucket Hat T01', 'Rp.50.000', '29 x 9 x 9 cm (p x l x t)\r\nBahan: Kain Mandar Balige\r\nInterlining Kain Blacu\r\nInterfacing: Fusing Resin', 1, 'Rp. 50.000', '', ''),
(57, 42, ' Topi  - Bucket Hat T01', 'Rp. 50.000', '29 x 9 x 9 cm (p x l x t)\r\nBahan: Kain Mandar Balige\r\nInterlining Kain Blacu\r\nInterfacing: Fusing Resin', 5, 'Rp. 250.000', '', ''),
(61, 44, ' Topi  - Bucket Hat T01', 'Rp.50.000', '29 x 9 x 9 cm (p x l x t)\r\nBahan: Kain Mandar Balige\r\nInterlining Kain Blacu\r\nInterfacing: Fusing Resin', 1, 'Rp. 50.000', 'Depan gerbang Institut Teknologi Del, Sitoluama, Kec. Balige, Toba, Sumatera Utara 22381', 'hatop ma broo'),
(62, 44, 'Tas Sandang Wanita TBS01', 'Rp. 160.000', 'Bahan Kain Mandar Balige\r\nInterlining: Kain Blacu\r\nInterfacing: Fusing Resin\r\nZipper\r\nTali Webbing Katun\r\nBadan 45 x 27 cm\r\nAlas 16 x 29 cm\r\nKantong dalam', 2, 'Rp. 320.000', 'Depan gerbang Institut Teknologi Del, Sitoluama, Kec. Balige, Toba, Sumatera Utara 22381', 'asdrtyui'),
(63, 42, ' Topi  - Bucket Hat T01', 'Rp.50.000', '29 x 9 x 9 cm (p x l x t)\r\nBahan: Kain Mandar Balige\r\nInterlining Kain Blacu\r\nInterfacing: Fusing Resin', 1, 'Rp. 50.000', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `harga_produk` decimal(10,2) NOT NULL,
  `deskripsi_produk` text DEFAULT NULL,
  `kategori` varchar(11) NOT NULL DEFAULT 'jual',
  `gambar_produk` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `harga_produk`, `deskripsi_produk`, `kategori`, `gambar_produk`) VALUES
(83, 'Topi - Bucket Hat T01', '50000.00', '29 x 9 x 9 cm (p x l x t) Bahan: Kain Mandar Balige Interlining Kain Blacu Interfacing: Fusing Resin', 'bestseller', 0x75706c6f6164732f67616d626172332e6a7067),
(85, 'Tas Sandang Wanita TBS01	', '160000.00', 'Bahan Kain Mandar Balige Interlining: Kain Blacu Interfacing: Fusing Resin Zipper Tali Webbing Katun Badan 45 x 27 cm Alas 16 x 29 cm Kantong dalam', 'jual', 0x75706c6f6164732f746173686974616d2e6a7067);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `jam_kerja` varchar(100) NOT NULL,
  `alamat_toko` varchar(100) NOT NULL,
  `whatsapp` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `facebook_toko` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `instagram_toko` varchar(100) NOT NULL,
  `whatsapp_pesan` varchar(100) NOT NULL,
  `google_maps` text NOT NULL,
  `logo_toko` blob NOT NULL,
  `gambar_owner` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `jam_kerja`, `alamat_toko`, `whatsapp`, `facebook`, `facebook_toko`, `instagram`, `instagram_toko`, `whatsapp_pesan`, `google_maps`, `logo_toko`, `gambar_owner`) VALUES
(1, 'Senin-Sabtu : 08:00-17:00', 'Alamat Toko: Jl. Raja Paindoan No.2, Balige', 'https://api.whatsapp.com/send?phone=6281262341479', 'https://www.facebook.com/chiko.pardede?mibextid=ZbWKwL', 'https://web.facebook.com/profile.php?id=100063679505196', 'https://www.instagram.com/pardedechiko/?hl=id', 'https://www.instagram.com/sekkacraft/', 'https://api.whatsapp.com/send?phone=6282211739374', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31892.0785609826!2d99.02791938678043!3d2.333493898809184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e055adf6fddeb%3A0xe920f9b1d555148e!2sSEKKA%20Craft%20Balige!5e0!3m2!1sid!2sid!4v1682489900544!5m2!1sid!2sid', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id_feedback`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `id_akun` (`id_akun`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id_feedback` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
