function searchProducts() {
    // Dapatkan kata kunci pencarian dari input
    var keyword = document.getElementById('searchInput').value.toLowerCase();

    // Cek apakah pencarian kosong
    if (keyword === '') {
        // Redirect ke halaman produk
        window.location.href = 'produk.php';
        return; // Hentikan eksekusi kode selanjutnya
    }

    // Buat objek XMLHttpRequest untuk mengirim permintaan ke server
    var xhr = new XMLHttpRequest();

    // Atur callback ketika permintaan selesai
    xhr.onload = function () {
        if (xhr.status === 200) {
            // Tangkap data produk dari respons server
            var products = JSON.parse(xhr.responseText);

            // Render ulang daftar produk berdasarkan hasil pencarian
            renderProductList(products);
        }
    };

    // Buat permintaan GET ke server dengan parameter pencarian
    xhr.open('GET', 'search_produk.php?keyword=' + keyword, true);
    xhr.send();
}



function renderProductList(products) {
    // Dapatkan elemen div yang berisi daftar produk
    var productListDiv = document.getElementById('productList');

    // Bersihkan konten sebelumnya
    productListDiv.innerHTML = '';

    // Loop melalui setiap produk dan tampilkan dalam elemen div
    products.forEach(function (product) {
        var productDiv = document.createElement('div');
        productDiv.className = 'produk col-md-5 mb-4 border mx-auto';
        productDiv.dataset.aos = 'fade-up';
        productDiv.style.display = 'flex';
        productDiv.style.flexWrap = 'wrap';
        productDiv.style.marginRight = '10px';
        productDiv.style.backgroundColor = '#ffffff';
        productDiv.style.borderRadius = '8px';
        productDiv.style.boxShadow = '0 4px 6px rgba(0.8, 0.8, 0.8, 0.8)';

        var imgDiv = document.createElement('div');
        imgDiv.className = 'col-md-6';
        imgDiv.style.alignItems = 'center';

        var img = document.createElement('img');
        img.className = 'produk-img';
        img.src = 'admind/' + product.gambar_produk;
        img.alt = '...';

        imgDiv.appendChild(img);
        productDiv.appendChild(imgDiv);

        var infoDiv = document.createElement('div');
        infoDiv.className = 'px-5 pb-5 col-md-6';

        var productName = document.createElement('h3');
        productName.className = 'produk-nama font-semibold mt-4';
        productName.textContent = product.nama_produk;

        var productDesc = document.createElement('h6');
        productDesc.className = 'produk-deskripsi text-sm text-gray-600 mt-2';
        productDesc.textContent = product.deskripsi_produk;

        var productHarga = document.createElement('span');
        productHarga.className = 'produk-harga text-3xl font-bold text-gray-900 dark:text-white mt-6';
        productHarga.textContent = 'Rp.' + (product.harga_produk);

        var productContainer = document.createElement('div');
        productContainer.style.display = 'flex';
        productContainer.style.flexDirection = 'column';
        productContainer.style.alignItems = 'center';

        var productBeli = document.createElement('a');
        productBeli.href = 'detail_produk.php?id=' + product.id_produk;
        productBeli.className = 'text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800';
        productBeli.textContent = 'Beli';

        infoDiv.appendChild(productName);
        infoDiv.appendChild(productDesc);
        infoDiv.appendChild(productHarga);
        productContainer.appendChild(infoDiv);
        productContainer.appendChild(productBeli);
        productDiv.appendChild(productContainer);

        productListDiv.appendChild(productDiv);
    });
}

