 // Activate Carousel
 var myCarousel = document.querySelector('#myCarousel')
 var carousel = new bootstrap.Carousel(myCarousel, {
   interval: 5000 // 5 detik
 })

 // Enable Carousel Indicators
 var indicators = document.querySelectorAll('.carousel-indicators li')
 indicators.forEach(function (indicator) {
   indicator.addEventListener('click', function () {
     carousel.to(parseInt(this.getAttribute('data-bs-slide-to')))
   })
 })

 // Enable Carousel Controls
 var prevButton = document.querySelector('.carousel-control-prev')
 prevButton.addEventListener('click', function () {
   carousel.prev()
 })
 var nextButton = document.querySelector('.carousel-control-next')
 nextButton.addEventListener('click', function () {
   carousel.next()
 })

 // Bestseller
 var cardImgTops = document.querySelectorAll('.card-img-top')
 cardImgTops.forEach(function (cardImgTop) {
   cardImgTop.addEventListener('click', function () {
     this.classList.toggle('card-img-top-clicked')
   })
 })