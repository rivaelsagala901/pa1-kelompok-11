<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>About</title>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel="stylesheet" href="css/footerr.css">
  <link rel="stylesheet" href="css/aboutt.css">
</head>
<style>
  body {
    padding-top: 60px;
  }

  @media (max-width: 992px) {
    body {
      padding-top: 0;
    }
  }
</style>

<body>
  <?php
  include("navbar.php");


  require "config.php";


  // Query untuk mengambil data produk yang akan diubah
  $query = "SELECT * FROM setting";
  $result = mysqli_query($conn, $query);
  $row = mysqli_fetch_array($result);


  ?>

  <section class="tarik">
    <div class="container">
      <div class="d-flex justify-content-center my-12">
        <img src="uploads/SEKKA CRAFT  hitam(2).png" alt="deskripsi-gambar" height="200" width="500" class="mx-auto" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000">
      </div>
      <div class="row text-center">
        <div>
          <h2 data-aos="fade-up" data-aos-time="1000"> SEKKA CRAFT </h2>
          <h4 data-aos="fade-up" data-aos-time="1000"> Jam Kerja </h4>
          <h5 data-aos="fade-up" data-aos-time="1000"><?php echo $row['jam_kerja'] ?></h5>
          <div id="date"></div>

          <script>
            function showDateTime() {
              var date = new Date();
              var options = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric'
              };
              var dateTime = date.toLocaleDateString('id-ID', options);
              document.getElementById("date").innerText = dateTime;
              setTimeout(showDateTime, 1000);
            }

            showDateTime();
          </script>
          <p><?php echo $row['alamat_toko'] ?></p>
        </div>


        <div class="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mt-5" data-aos="fade-down" data-aos-duration="2000">
          <div class="flex justify-end px-4 pt-4">
          </div>
          <div class="flex flex-col items-center pb-10">
            <img class="w-24 h-24 mb-3 rounded-full shadow-lg" src="uploads/ibu pasu dame.jpg" alt="pendiri" />
            <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">Pendiri Sekka Craft Balige</h5>
            <span class="text-sm text-gray-500 dark:text-gray-400">Ibu Pasu Dame Pardede</span>
            <div class="flex mt-4 space-x-3 md:mt-6">
              <a class="bi bi-facebook" href="<?php echo $row['facebook'] ?>" target="_blank" class="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"> Add friend</a>
              <a class="bi bi-whatsapp" href="<?php echo $row['whatsapp'] ?>" target="_blank" class="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-gray-900 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-700 dark:focus:ring-gray-700"> Message</a>
              <a class="bi bi-instagram" href="<?php echo $row['instagram'] ?>" target="_blank" class="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-gray-900 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-700 dark:focus:ring-gray-700"> Message</a>
            </div>
          </div>
        </div>


        <div class="container mt-5">
          <div class="row">
            <div class="col-lg-6">
              <h2 class="mb-4" data-aos="fade-right">Tentang Kami</h2>
              <p class="text-muted" data-aos="fade-up">
                Pada tahun 2018, Ibu Pasu Dame Pardede yang merupakan seorang pecinta kerajinan tangan, memiliki
                keinginan untuk menghasilkan kerajinan tangan yang dapat dijadikan sebagai oleh-oleh khas dari
                Balige. Ibu Pasu memulai bisnis kerajinan tangan di rumahnya, ia menciptakan berbagai produk seperti
                tas, bucket hat, pouch, dompet kotak, dan masih banyak lagi dengan berbahan dasar kain khas dari
                Balige yaitu mandar Balige.
              </p>
              <p class="text-muted" data-aos="fade-up">
                Produk yang dihasilkan mulai menarik perhatian orang-orang di lingkungannya. Beliau memanfaatkan
                platform online dan media sosial untuk memperkenalkan produk-produknya kepada lebih banyak orang.
                Dengan umpan balik positif dan permintaan yang meningkat, Ibu Pasu mulai kesulitan dalam memenuhi
                pesanan yang semakin banyak. Lalu ia memutuskan untuk merekrut beberapa karyawan yang berbakat untuk
                membantu produksi dan pemasaran.
              </p>
            </div>
            <div class="col-lg-6">
              <h2 class="mb-4" data-aos="fade-left">Workshop dan Kelas</h2>
              <p class="text-muted" data-aos="fade-up">
                Ibu Pasu juga aktif mengadakan workshop dan kelas kerajinan tangan untuk masyarakat setempat,
                terutama untuk para pelajar yang ingin belajar membuat kerajinan tangan. Dengan harapan dapat
                membagikan pengetahuannya kepada orang lain.
              </p>
            </div>
          </div>
        </div>
        <iframe class="mb-5" src="<?php echo $row['google_maps'] ?>" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      </div>

  </section>


  <!-- footer -->
  <?php
  include("footer.php");
  ?>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</body>

</html>