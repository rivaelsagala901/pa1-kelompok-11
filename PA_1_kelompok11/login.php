<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <style>
        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .card {
            max-width: 800px;
            width: 90%;
            margin: 0 auto;
            border-radius: 45px;
        }
    </style>
</head>

<body class="header finisher-header">
    <div class="container" data-aos="fade-down" >
        <div class="card" style="background-color: #ffffff; backdrop-filter: blur(80px); -webkit-backdrop-filter: blur(80px);">
            <div class="text-center p-4">
                <img src="uploads/logo sekka.jpg" class="img-fluid" alt="..." width="300">
                <h3>Selamat Datang di Website Sekka Craft</h3>
                <h2>Kabupaten Toba</h2>
                <p>Crafting your dreams into reality</p>
                <br>
                <a href="login_user.php" class="btn btn-secondary m-1">Login</a>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/finisher-header.es5.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        new FinisherHeader({
            "count": 6,
            "size": {
                "min": 1100,
                "max": 1300,
                "pulse": 0
            },
            "speed": {
                "x": {
                    "min": 0.1,
                    "max": 0.3
                },
                "y": {
                    "min": 0.1,
                    "max": 0.3
                }
            },
            "colors": {
                "background": "#38e5d4",
                "particles": [
                    "#6bd6ff",
                    "#60dac6",
                    "#43596c"
                ]
            },
            "blending": "overlay",
            "opacity": {
                "center": 1,
                "edge": 0.1
            },
            "skew": 0,
            "shapes": [
                "c"
            ]
        });
    </script>
</body>

</html>