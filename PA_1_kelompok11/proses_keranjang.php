<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>

<body>
    <?php
    session_start(); // Memulai sesi, pastikan ini berada di awal kode
    require('config.php');

    // Ambil data dari form
    $nama_produk = $_POST['nama_produk'];
    $harga = $_POST['harga'];
    $deskripsi_produk = $_POST['deskripsi_produk'];
    $jumlah = $_POST['jumlah'];
    $total_harga = $_POST['total_harga'];
    $lokasi = $_POST['lokasi'];
    $pesan = $_POST['pesan'];

    // Periksa koneksi
    if (mysqli_connect_errno()) {
        echo "Gagal terhubung ke MySQL: " . mysqli_connect_error();
        exit();
    }

    // Ambil nama akun dari sesi yang sudah login
    $user_name = $_SESSION['username'];

    // Query untuk mengambil id_akun berdasarkan nama_akun
    $query = "SELECT id_akun FROM akun WHERE username = '$user_name'";
    $_SESSION['masuk_keranjang'] = 'masuk';
    // Eksekusi query
    $result = mysqli_query($conn, $query);

    if ($result && mysqli_num_rows($result) > 0) {

        $row = mysqli_fetch_assoc($result);
        $id_akun = $row['id_akun'];

        // Query untuk memasukkan data ke dalam tabel keranjang
        $query_insert = "INSERT INTO keranjang (id_akun, nama_produk, harga, deskripsi_produk, jumlah, total_harga, lokasi, pesan) VALUES ('$id_akun', '$nama_produk', '$harga', '$deskripsi_produk', '$jumlah', '$total_harga', '$lokasi', '$pesan')";



        // Eksekusi query INSERT
        if (mysqli_query($conn, $query_insert)) { ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Pesanan berhasil di tambahkan ke Keranjang',
                    onClose: function() {
                        window.location.href = "keranjang.php";
                    }
                });
            </script>
    <?php exit();
        }
    } else {
        echo "Error: " . $query_insert . "<br>" . mysqli_error($conn);
    }



    // Tutup koneksi ke database
    mysqli_close($conn);

    ?>

</body>

</html>
<?php
