<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>sekka craft</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/footerr.css">
    <link href="assets/css/stylee.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <style>
        .card-img-top {
            width: 100%;
            height: 100%;
            /* mengatur tinggi gambar */
            object-fit: cover;
            /* memastikan gambar tidak terpotong dan menyesuaikan ukuran */
        }

        body {
            padding-top: 70px;
            background-color: #F3F4F6;
        }

        @media (max-width: 992px) {
            body {
                padding-top: 0;
            }
        }
    </style>
</head>

<body style="background-color: #F3F4F6;">
    <?php
    include("navbar.php");
    ?>
    <section id="hero1">
        <div class="hero-container header finisher-header h-auto" data-aos="zoom-in" data-aos-delay="100">
            <h1>Welcome to Sekka Craft</h1>
            <h2>Inspiring creativity through handmade crafts</h2>
            <a href="#contact-section" class="btn-get-started">Contact</a>
        </div>
    </section>

    <!-- border -->
    <section class="tarik" style="background-color: #1F2937;">
        <div class="container">
            <div class="row">
                <div class="col-md-6" data-aos="fade-right">
                    <img src="uploads/11.png" alt="Banner Sekka Craft" class="img-fluid">
                </div>
                <div class="col-md-6 text-light " data-aos="fade-left" data-aos-delay="200">
                    <h2> Selamat Datang di Sekka Craft </h2>
                    <p> Kami menyediakan berbagai jenis barang kerajinan tangan yang unik dan berkualitas untuk
                        Anda. </p>
                </div>
            </div>
        </div>
    </section>


    <!-- ======= Hero Section ======= -->
    <section class="hero d-flex align-items-center section-bg bg-light mt-5">
        <div class="container">
            <div class="row justify-content-between gy-5">
                <div class="col-lg-5 order-2 order-lg-1 d-flex flex-column justify-content-center align-items-center align-items-lg-start text-center text-lg-start">
                    <h1 class="fw-bold mb-4" data-aos="fade-up" style="font-size: 3rem;">Karya seni dari <br>tangan-tangan terampil</h1>
                    <p class="mb-4" data-aos="fade-up" style="font-size: 1.2rem; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;" data-aos-delay="100">Ketelitian dan keahlian pengrajin Sekka Craft di Balige menghasilkan karya seni yang indah dan bernilai tinggi.</p>
                </div>
                <div class="col-lg-5 order-1 order-lg-2 text-center text-lg-start">
                    <img src="uploads/Rectangle 20.png" class="img-fluid" alt="" data-aos="zoom-out" data-aos-delay="300" style="width: 100%; height: auto;">
                </div>
            </div>
        </div>
    </section>



    <!-- corousel  -->
    <section id="hero">
        <div class="hero-container">
            <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">
                    <!-- Slide 1 -->
                    <div class="carousel-item active">
                        <div class="carousel-container" style="background-color: #84E1BC;">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown">Kerajinan tangan Sekka Craft <br> sebuah warisan budaya yang harus dijaga</h2>
                                <p class="animate__animated animate__fadeInUp">Kerajinan tangan Sekka Craft Balige merupakan bukti nyata bahwa keindahan dapat dihasilkan dari bahan-bahan yang sederhana.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 2 -->
                    <div class="carousel-item">
                        <div class="carousel-container" style="background-color: #BCF0DA;">
                            <div class="carousel-content">
                                <h2 class="animate__animated fanimate__adeInDown">Kerajinan tangan Sekka Craft <br> kreativitas dan ketelitian bergabung menjadi satu.</h2>
                                <p class="animate__animated animate__fadeInUp">Ketelitian dan keahlian pengrajin menghasilkan karya seni yang indah dan bernilai tinggi.</p>
                            </div>
                        </div>
                    </div>


                    <!-- Slide 3 -->
                    <div class="carousel-item">
                        <div class="carousel-container" style="background-color: #84E1BC;">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown ">Karya seni yang indah<br>dan bernilai tinggi</h2>
                                <p class="animate__animated animate__fadeInUp">Dalam setiap karya Sekka Craft di Balige terdapat keunikan dan keindahan yang sulit ditiru oleh teknologi modern.</p>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                </a>

                <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                </a>

            </div>
        </div>
    </section>

    <!-- banner -->


    <div id="default-carousel" class="relative w-full" data-carousel="slide">
        <!-- Carousel wrapper -->
        <div class="relative h-full overflow-hidden rounded-lg md:h-96" style="height: 700px;">
            <!-- Item 1 -->
            <div class="hidden duration-700 ease-in-out" data-carousel-item>
                <img src="gambar/2.png" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
            </div>
            <!-- Item 2 -->
            <div class="hidden duration-700 ease-in-out" data-carousel-item>
                <img src="gambar/3.png" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
            </div>
            <!-- Item 3 -->
            <div class="hidden duration-700 ease-in-out" data-carousel-item>
                <img src="gambar/4.png" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
            </div>
            <!-- Item 4 -->
            <div class="hidden duration-700 ease-in-out" data-carousel-item>
                <img src="gambar/1.png" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
            </div>
        </div>
        <!-- Slider indicators -->
        <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
            <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
            <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
            <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
            <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
        </div>
        <!-- Slider controls -->
        <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev>
            <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path>
                </svg>
                <span class="sr-only">Previous</span>
            </span>
        </button>
        <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next>
            <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path>
                </svg>
                <span class="sr-only">Next</span>
            </span>
        </button>
    </div>


    <!-- bestseller -->
    <div class="container-fluid p-5">
        <div class="row">
            <h1 class="text-center fw-bolder text-black" data-aos="fade-down">Best Seller</h1>

            <?php
            require('config.php');
            $query = "SELECT * FROM produk WHERE kategori='bestseller'";
            $result = mysqli_query($conn, $query);

            // Periksa apakah ada produk yang ditemukan
            if (mysqli_num_rows($result) > 0) {
                // Loop melalui setiap produk dan menampilkannya
                while ($row = mysqli_fetch_assoc($result)) {

            ?>
                    <div class="col-md-4 mb-5 mx-auto" data-aos="fade-up" style="display: flex; flex-wrap: wrap; margin-right: 10px;">
                        <a href="produk.php" class="flex flex-col items-center bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
                            <div class="col-md-6" style="align-items: center;">
                                <img src="admind/<?php echo $row['gambar_produk']; ?>" class="produk-img" alt="...">
                            </div>
                            <div class="flex flex-col justify-between p-4 leading-normal">
                                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white"><?php echo $row['nama_produk'] ?></h5>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400"><?php echo $row['deskripsi_produk'] ?></p>
                            </div>
                        </a>
                    </div>
            <?php
                }
            }

            // Tutup koneksi ke database
            mysqli_close($conn);
            ?>

        </div>
    </div>



    <!-- footer -->


    <div id="contact-section">
        <?php include 'footer.php'; ?>
    </div>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <script src="js/finisher-header.es5.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        new FinisherHeader({
            "count": 90,
            "size": {
                "min": 1,
                "max": 20,
                "pulse": 0
            },
            "speed": {
                "x": {
                    "min": 0,
                    "max": 0.4
                },
                "y": {
                    "min": 0,
                    "max": 0.1
                }
            },
            "colors": {
                "background": "#24d891",
                "particles": [
                    "#ffffff",
                    "#87ddfe",
                    "#acaaff",
                    "#1bffc2",
                    "#f88aff"
                ]
            },
            "blending": "screen",
            "opacity": {
                "center": 0,
                "edge": 0.4
            },
            "skew": 0,
            "shapes": [
                "c",
                "s",
                "t"
            ]
        });
    </script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
    <script setup>
        import {
            onMounted
        } from 'vue'
        import {
            initFlowbite
        } from 'flowbite'

        // initialize components based on data attribute selectors
        onMounted(() => {
            initFlowbite();
        })
    </script>

    <!-- Penambahan file JavaScript untuk Bootstrap 5 -->
</body>

</html>