<!DOCTYPE html>
<html>

<head>
    <title>Produk Sekka Craft</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/produk.css">
    <link rel="stylesheet" href="css/bar.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <style>
        a {
            text-decoration: none;
        }

        body {
            padding-top: 100px;
        }

        @media (max-width: 992px) {
            body {
                padding-top: 0;
            }
        }
    </style>
</head>

<body>
    <?php
    session_start();
    // Periksa apakah pengunjung sudah login atau belum
    if (!isset($_SESSION['akun_id'])) { ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'warning',
                title: 'Login diperlukan',
                text: 'Anda harus masuk terlebih dahulu sebelum membuat pemesanan!',
                onClose: function() {
                    window.location.href = "login.php";
                }
            });
        </script>
    <?php exit();
    } ?>
    <?php

    if (isset($_SESSION['username'])) {
        $username = $_SESSION['username'];
    } else {
        $username = 'Belum Login';
    }
    ?>

    <!-- navbar -->

    <nav class="bg-black border-gray-200 dark:bg-gray-900 fixed-top">
        <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto  bg-black ">
            <a href="about.php" class="flex items-center text-light">
                <img src="uploads/SEKKA CRAFT  hitam(2).png" class="w-28 h-11 rounded-full" alt="Flowbite Logo" />
                <h1><span class="self-center p-4 text-2xl font-semibold whitespace-nowrap dark:text-white">SEKKA CRAFT</span></h1>
            </a>
            <div class="flex items-center md:order-2">
                <button data-tooltip-target="tooltip-profile" type="button" class="flex mr-3 text-sm bg-gray-800 rounded-full md:mr-0 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" id="user-menu-button" aria-expanded="false" data-dropdown-toggle="user-dropdown" data-dropdown-placement="bottom">
                    <span class="sr-only">Open user menu</span>
                    <img class="w-28 h-10 rounded-full " src="uploads/gambarkontak1.jpg" alt="user photo">
                </button>
                <div id="tooltip-profile" role="tooltip" class="absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-black transition-opacity duration-300 bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip bg-light">
                    Profile
                    <div class="tooltip-arrow" data-popper-arrow></div>
                </div>

                <!-- Dropdown menu -->
                <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600" id="user-dropdown">
                    <div class="px-4 py-3">
                        <span class="block text-sm text-gray-900 dark:text-white"><?php echo $username; ?></span>
                    </div>
                    <ul class="py-2" aria-labelledby="user-menu-button">
                        <li>
                            <a href="login_user.php" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"><i class="bi bi-person-fill"></i> Login</a>
                        </li>
                        <li>
                            <a href="logout.php" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"><i class="bi bi-box-arrow-right"></i> Logout</a>
                        </li>
                    </ul>
                </div>



                <button data-collapse-toggle="mobile-menu-2" type="button" class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mobile-menu-2" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="mobile-menu-2" data-aos="fade-up" data-aos-anchor-placement="bottom-center">
                <ul class="flex flex-col font-medium p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                    <li>
                        <a href="home.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-house-fill"></i> Home</a>
                    </li>
                    <li>
                        <a href="produk.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-bag-check-fill"></i> Produk</a>
                    </li>
                    <li>
                        <a href="feedback.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-chat-left-dots-fill"></i> Feedback</a>
                    </li>
                    <li>
                        <a href="about.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-file-earmark-person-fill"></i> About</a>
                    </li>
                    <li>
                        <a href="Keranjang.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-cart4"></i> Keranjang</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container mt-5">
        <div class="row">
            <?php
            // Koneksi ke database
            require "config.php";

            // Cek apakah pengunjung sudah login
            if (isset($_SESSION['akun_id'])) {
                // Pengunjung sudah login, bisa membeli
                // Query untuk mengambil data produk dengan id yang diberikan
                if (isset($_GET['id'])) {
                    $id = $_GET['id'];

                    $query = "SELECT * FROM produk WHERE id_produk=$id";
                    $result = mysqli_query($conn, $query);

                    // Periksa apakah ada produk yang ditemukan
                    if (mysqli_num_rows($result) > 0) {
                        $row = mysqli_fetch_assoc($result);
            ?>



                        <div class="row">
                            <div class="col-md-6" data-aos="fade-down" data-aos-duration="1000">
                                <img src="admind/<?php echo $row['gambar_produk']; ?>" alt="<?php echo $row['nama_produk']; ?>" class="img-fluid" width="400" height="200">
                            </div>
                            <div class="col-md-6" data-aos="flip-right" data-aos-duration="500" data-aos-delay="500">
                                <h2 name="nama_produk"><?php echo $row['nama_produk']; ?></h2>
                                <p class="text-muted"><?php echo $row['deskripsi_produk']; ?></p>
                                <hr>
                                <h3>Rp. <span id="harga_produk"><?php echo number_format($row['harga_produk'], 0, ',', '.'); ?></span> /satuan</h3>
                                <form method="post" action="proses_keranjang.php">
                                    <input type="hidden" name="nama_produk" value="<?php echo htmlspecialchars($row['nama_produk']); ?>">
                                    <input type="hidden" name="harga" value="Rp.<?php echo number_format($row['harga_produk'], 0, ',', '.'); ?>">
                                    <input type="hidden" name="deskripsi_produk" value="<?php echo $row['deskripsi_produk']; ?>">
                                    <div class="form-group">
                                        <label for="jumlah">Jumlah:</label>
                                        <input type="number" name="jumlah" id="jumlah" class="form-control" min="1" value="1" style="width: 85px;" onchange="hitungTotalHarga()">
                                    </div>
                                    <div class="form-group">
                                        <label for="total_harga">Total Harga:</label>
                                        <input type="text" name="total_harga" id="total_harga" class="form-control" value="Rp. <?= number_format($row['harga_produk'], 0, ',', '.') ?>" style="width: 200px;" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasi">Masukkan Lokasi</label>
                                        <textarea name="lokasi" id="lokasi" class="form-control" placeholder="Masukkan lokasi pengiriman" style="width: 400px;" required></textarea>
                                    
                                    </div>

                                    <div class="form-group">
                                        <label for="pesan">Pesan</label>
                                        <textarea name="pesan" id="pesan" class="form-control" placeholder="Masukkan pesan jika ada" style="width: 400px;"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary mt-4">Masuk keranjang</button>
                                </form>
                            </div>
                <?php
                    }
                }
            }

            // Tutup koneksi ke database
            mysqli_close($conn);
                ?>

                <?php if (isset($_SESSION['masuk_keranjang'])) : ?>
                    <div class="edit-data" data-editdata="<?php echo $_SESSION['masuk_keranjang']; ?>"></div>
                <?php unset($_SESSION['masuk_keranjang']);
                endif; ?>

                <script>
                    function hitungTotalHarga() {
                        var jumlah = document.getElementById("jumlah").value;
                        var harga_produk = parseFloat(document.getElementById("harga_produk").innerText.replace(/[^\d]/g, ''));
                        var total_harga = jumlah * harga_produk;

                        document.getElementById("total_harga").value = "Rp. " + formatRupiah(total_harga);
                        document.getElementsByName("harga")[0].value = "Rp. " + formatRupiah(harga_produk);
                    }

                    function formatRupiah(angka) {
                        var rupiah = angka.toString().split('').reverse().join('');
                        var ribuan = rupiah.match(/\d{1,3}/g);
                        ribuan = ribuan.join('.').split('').reverse().join('');
                        return ribuan;
                    }
                </script>


                <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
                <script>
                    AOS.init();
                </script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/js/bootstrap.bundle.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</body>

</html>