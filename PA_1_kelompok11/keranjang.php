<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Keranjang</title>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="css/footerr.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <style>
        body {
            padding-top: 150px;
        }

        @media (max-width: 992px) {
            body {
                padding-top: 0;
            }
        }
    </style>

</head>

<body>

    <?php
    session_start();
    // Periksa apakah pengunjung sudah login atau belum
    if (!isset($_SESSION['akun_id'])) { ?>
        <script type="text/javascript">
            Swal.fire({
                title: 'Tidak ada produk',
                onClose: function() {
                    window.location.href = "produk.php";
                }
            });
        </script>

    <?php exit();
    } ?>
    <?php

    if (isset($_SESSION['username'])) {
        $username = $_SESSION['username'];
    } else {
        $username = 'Belum Login';
    }
    ?>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <style>
        a {
            text-decoration: none;
        }
    </style>

    <nav class="bg-black border-gray-200 dark:bg-gray-900 fixed-top">
        <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto  bg-black ">
            <a href="about.php" class="flex items-center text-light">
                <img src="uploads/SEKKA CRAFT  hitam(2).png" class="w-28 h-11 rounded-full" alt="Flowbite Logo" />
                <h1><span class="self-center p-4 text-2xl font-semibold whitespace-nowrap dark:text-white">SEKKA CRAFT</span></h1>
            </a>
            <div class="flex items-center md:order-2">
                <button data-tooltip-target="tooltip-profile" type="button" class="flex mr-3 text-sm bg-gray-800 rounded-full md:mr-0 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" id="user-menu-button" aria-expanded="false" data-dropdown-toggle="user-dropdown" data-dropdown-placement="bottom">
                    <span class="sr-only">Open user menu</span>
                    <img class="w-28 h-10 rounded-full " src="uploads/gambarkontak1.jpg" alt="user photo">
                </button>
                <div id="tooltip-profile" role="tooltip" class="absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-black transition-opacity duration-300 bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip bg-light">
                    Profile
                    <div class="tooltip-arrow" data-popper-arrow></div>
                </div>

                <!-- Dropdown menu -->
                <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600" id="user-dropdown">
                    <div class="px-4 py-3">
                        <span class="block text-sm text-gray-900 dark:text-white"><?php echo $username; ?></span>
                    </div>
                    <ul class="py-2" aria-labelledby="user-menu-button">
                        <li>
                            <a href="login_user.php" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"><i class="bi bi-person-fill"></i> Login</a>
                        </li>
                        <li>
                            <a href="logout.php" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"><i class="bi bi-box-arrow-right"></i> Logout</a>
                        </li>
                    </ul>
                </div>

                <button data-collapse-toggle="mobile-menu-2" type="button" class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mobile-menu-2" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="mobile-menu-2" data-aos="fade-up" data-aos-anchor-placement="bottom-center">
                <ul class="flex flex-col font-medium p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                    <li>
                        <a href="home.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-house-fill"></i> Home</a>
                    </li>
                    <li>
                        <a href="produk.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-bag-check-fill"></i> Produk</a>
                    </li>
                    <li>
                        <a href="feedback.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-chat-left-dots-fill"></i> Feedback</a>
                    </li>
                    <li>
                        <a href="about.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-file-earmark-person-fill"></i> About</a>
                    </li>
                    <li>
                        <a href="Keranjang.php" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><i class="bi bi-cart4"></i> Keranjang</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <h2>Data Keranjang</h2>


        <?php
        // Periksa apakah pengunjung sudah login atau belum
        if (!isset($_SESSION['akun_id'])) {
            // Jika belum login, redirect ke halaman login
            header("Location: produk.php"); // Ganti "login.php" dengan halaman login yang sesuai
        }


        require('config.php');


        // Periksa koneksi
        if (mysqli_connect_errno()) {
            echo "Gagal terhubung ke MySQL: " . mysqli_connect_error();
            exit();
        }

        // Ambil nama akun dari sesi yang sudah login
        $user_name = $_SESSION['username'];

        // Query untuk mengambil id_akun berdasarkan nama_akun
        $query = "SELECT id_akun FROM akun WHERE username = '$user_name'";

        // Eksekusi query
        $result = mysqli_query($conn, $query);



        if ($result && mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            $id_akun = $row['id_akun'];

            // Query untuk menampilkan data dari tabel keranjang
            $query_select = "SELECT * FROM keranjang WHERE id_akun = '$id_akun' ORDER BY id_keranjang DESC";
            $result_select = mysqli_query($conn, $query_select);

            if ($result_select && mysqli_num_rows($result_select) > 0) {
        ?>
                <div class="overflow-x-auto">
                    <table class="table table-striped table-responsive">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Harga</th>
                                <th>Deskripsi Produk</th>
                                <th>Jumlah</th>
                                <th>Total Harga</th>
                                <th>Lokasi</th>
                                <th>Pesan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 1;
                            while ($row_select = mysqli_fetch_assoc($result_select)) :
                            ?>
                                <tr>
                                    <td><?= $counter ?></td>
                                    <td><?= $row_select['nama_produk'] ?></td>
                                    <td><?= $row_select['harga'] ?></td>
                                    <td><?= $row_select['deskripsi_produk'] ?></td>
                                    <td><?= $row_select['jumlah'] ?></td>
                                    <td><?= $row_select['total_harga'] ?></td>
                                    <td><?= $row_select['lokasi'] ?></td>
                                    <td><?= $row_select['pesan'] ?></td>
                                    <td><a href="hapus_keranjang.php?id=<?= $row_select['id_keranjang'] ?>" class="btn btn-danger">Hapus</a></td>
                                </tr>
                            <?php
                                $counter++;
                            endwhile;
                            ?>
                        </tbody>
                    </table>
                </div>


                <div class="text-center">
                    <a href="proses_pemesanan.php" class="btn btn-primary">Pesan Sekarang</a>
                </div>


            <?php
            } else { ?>
                <script type="text/javascript">
                    Swal.fire({
                        title: 'Tidak ada produk',
                        onClose: function() {
                            window.location.href = "produk.php";
                        }
                    });
                </script>

        <?php
            }
        }



        // Tutup koneksi ke database
        mysqli_close($conn);
        ?>
    </div>

    <?php if (isset($_SESSION['terhapus'])) : ?>
        <div class="edit-data" data-editdata="<?php echo $_SESSION['terhapus']; ?>"></div>
    <?php unset($_SESSION['terhapus']);
    endif; ?>

    <script src=" https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</body>

</html>

<script>
    $(document).ready(function() {
        const editdata = $('.edit-data').data('editdata');
        if (editdata) {
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: "Data berhasil di hapus"
            });
        }
    });
</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>