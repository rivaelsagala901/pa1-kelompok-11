<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Feedback</title>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel="stylesheet" href="css/footerr.css">
  <link rel="stylesheet" href="css/feedbac.css">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <style>
    body {
      padding-top: 150px;
    }

    @media (max-width: 992px) {
      body {
        padding-top: 0;
      }
    }
  </style>

</head>

<body>
  <?php
  include("navbar.php");
  ?>
  <div class="container">
    <div class="card" style="background-color: #E5E7EB;">
      <div class="card-header mt-5" style="background-color: #E5E7EB;">
        <h1 class="card-title mb-0 mt-1 text-center" style="background-color: #E5E7EB;">Form Feedback Sekka Craft</h1>
      </div>
      <div class="d-flex justify-content-center p-5 " style="background-color: #E5E7EB;">
        <form method="post" action="feedback_proses.php" id="feedback-form" style="width:50%;">
          <div class="mb-3">
            <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="Masukkan nama Anda" required>
          </div>
          <div class="mb-3">
            <input type="email" class="form-control form-control-lg" id="email" name="email" placeholder="Masukkan email Anda" required>
          </div>
          <div class="mb-3">
            <textarea class="form-control form-control-lg" id="feedback" name="feedback" rows="5" placeholder="Masukkan feedback Anda" required></textarea>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg rounded-pill">Submit <i class="bi bi-send-fill"></i></button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="feedback-container">
      <h1 class="card-title mb-0 mt-10 text-center">Feedback Sekka Craft</h1>
      <?php
      require "config.php";
      // Query untuk mengambil data produk
      $sql = "SELECT nama, email, komentar, waktu FROM feedback WHERE aksi='tampil' ORDER BY id_feedback DESC";


      $result = mysqli_query($conn, $sql);


      // Periksa apakah ada produk yang ditemukan
      if (mysqli_num_rows($result) > 0) {
        // Loop melalui setiap produk dan menampilkannya
        while ($row = mysqli_fetch_assoc($result)) {
          $waktu = strtotime($row['waktu']);
          $tanggal = date('d', $waktu);
          $bulan = date('M', $waktu);
          $hari = date('l', $waktu);
          $tahun = date('Y', $waktu);


      ?>
          <div class="mt-4 mb-8" data-aos="fade-down">
            <div class="row">
              <div class="col-md-1">
                <img class="h-10 w-10 rounded-full" src="https://ui-avatars.com/api/?name=<?php echo urlencode($row['nama']) ?>&background=0D8ABC&color=fff" alt="">
              </div>
              <div class="col-md-10">
                <div>
                  <div class="text-sm font-medium text-gray-900"><?php echo $row['nama'] ?></div>
                  <div class="mt-1 text-sm text-gray-700">
                    <p class="text-gray-500 font-medium break-all" style="word-break: break-word;">Komentar: <?php echo $row['komentar'] ?></p>
                  </div>
                </div>
                <div class="mt-2 text-sm">
                  <span class="text-gray-500 font-medium"><?php echo $row['email'] ?></span>
                  <span class="text-gray-500 font-medium"><?php echo $hari . ', ' . $tanggal . ' ' . $bulan . ' ' . $tahun; ?></span>
                </div>
                <span class="text-gray-500 font-medium"><?php echo $row['waktu'] ?></span>
              </div>
            </div>
          </div>

      <?php
        }
      }
      // Tutup koneksi ke database
      mysqli_close($conn);
      ?>
    </div>
  </div>


  <!-- footer -->
  <?php
  include("footer.php");
  ?>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</body>

</html>