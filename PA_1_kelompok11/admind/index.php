  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <?php
    include("dashboard.php");
    ?>

    <!-- offcanvas -->
    <main class="mt-5 pt-3">
      <div class="container-fluid text-center">
        <div class="row">
          <div class="col-md-12 fw-bold fs-3">Selamat Datang di Halaman Admin Sekka Craft</div>
        </div>
        <div class="row">
          <div class="col-md-12 fw-bold fs-3">Dashboard</div>
        </div>
        <div class="row text-center">
          <?php
          include('../config.php');
          // Menghitung jumlah produk
          $sql_produk = "SELECT COUNT(*) AS total_produk FROM produk";
          $result_produk = mysqli_query($conn, $sql_produk);
          $data_produk = mysqli_fetch_assoc($result_produk);
          $total_produk = $data_produk['total_produk'];

          // Menghitung jumlah feedback
          $sql_feedback = "SELECT COUNT(*) AS total_feedback FROM feedback";
          $result_feedback = mysqli_query($conn, $sql_feedback);
          $data_feedback = mysqli_fetch_assoc($result_feedback);
          $total_feedback = $data_feedback['total_feedback'];

          // Menghitung jumlah akun pengguna
          $sql_pengguna = "SELECT COUNT(*) AS total_pengguna FROM akun WHERE roll='user'";
          $result_pengguna = mysqli_query($conn, $sql_pengguna);
          $data_pengguna = mysqli_fetch_assoc($result_pengguna);
          $total_pengguna = $data_pengguna['total_pengguna'];
          ?>

          <div class="col-md-4">
            <a href="barang.php">
              <div class="card text-white  mb-3" style="max-width: 18rem;background-color: #84E1BC;">
                <div class="card-header">Jumlah Produk</div>
                <div class="card-body">
                  <h5 class="card-title" style="font-size: 50px; font-weight: bold;"><?php echo $total_produk; ?></h5>
                  <p class="card-text">Produk yang tersedia.</p>
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-4">
            <a href="feedback.php">
              <div class="card text-white  mb-3" style="max-width: 18rem;background-color: #84E1BC;">
                <div class="card-header">Jumlah Feedback</div>
                <div class="card-body">
                  <h5 style="font-size: 50px; font-weight: bold;" class="card-title"><?php echo $total_feedback; ?></h5>
                  <p class="card-text">Feedback yang telah diterima.</p>
                </div>
              </div>

            </a>
          </div>
          <div class="col-md-4">
            <a href="akun_user.php">
              <div class="card text-white  mb-3" style="max-width: 18rem;background-color: #84E1BC;">
                <div class="card-header">Jumlah Akun</div>
                <div class="card-body">
                  <h5 style="font-size: 50px; font-weight: bold;" class="card-title"><?php echo $total_pengguna; ?></h5>
                  <p class="card-text">Akun yang telah terdaftar.</p>
                </div>
              </div>

            </a>
          </div>
        </div>
      </div>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>

  </html>