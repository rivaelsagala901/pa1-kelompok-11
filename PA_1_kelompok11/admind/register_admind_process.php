<?php
require '../config.php';

$username = $_POST['username'];
$password = $_POST['password'];
$roll = 'admin';
$password_confirmation = $_POST['password_confirmation'];

// Validasi username
$check_query = "SELECT * FROM akun WHERE username='$username'";
$check_result = mysqli_query($conn, $check_query);

if (mysqli_num_rows($check_result) > 0) {
	// Jika username sudah ada di dalam database, tampilkan pesan alert dan kembali ke halaman daftar
	echo "<script>alert('Username sudah digunakan! Silakan gunakan username lain.');</script>";
	echo "<script>window.location.href='daftar_admind.php';</script>";
	exit();
} else {
	if ($password != $password_confirmation) {
		// Jika password tidak sama dengan konfirmasi password, tampilkan pesan alert dan kembali ke halaman daftar
		echo "<script>alert('Konfirmasi password tidak sama! Silakan ulangi pengisian.');</script>";
		echo "<script>window.location.href='daftar_admind.php';</script>";
		exit();
	} else {

		// Mengenkripsi password menggunakan bcrypt
		$hashedPassword = password_hash($password, PASSWORD_BCRYPT);
		$query_sql = "INSERT INTO akun (username, password,roll) VALUES ('$username', '$hashedPassword','$roll')";

		if (mysqli_query($conn, $query_sql)) {
			header("Location: index.php");
			exit();
		} else {
			echo "Pendaftaran akun gagal: " . mysqli_error($conn);
			exit();
		}
	}
}
