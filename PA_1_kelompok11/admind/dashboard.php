<?php
session_start();

// Periksa apakah pengguna telah login sebagai admin
if (!isset($_SESSION['username'])) {
  header("Location:../login.php");
  exit();
}
?>

<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container-fluid">
    <!-- offcanvas trigger -->
    <button class="navbar-toggler me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- offcanvas trigger -->
    <a class="navbar-brand fw-bold text-uppercase me-auto" href="#">SEKKA CRAFT</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    </div>
  </div>
</nav>
<!-- navbar -->


<!-- offcanvas -->
<div class="offcanvas offcanvas-start bg-dark text-white sidebar-nav" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
  <div class="offcanvas-body p-0">
    <nav class="navbar-dark">
      <ul class="navbar-nav">
        <li>
          <div class="text-muted small fw-bold text-uppercase px-3">
            CORE
          </div>
        </li>
        <li>
          <a href="index.php" class="nav-link px-3 active">
            <span class="me-2">
              <i class="bi bi-house-fill"></i>
            </span>
            <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="setting.php" class="nav-link px-3 active">
            <span class="me-2">
              <i class="bi bi-gear"></i>
            </span>
            <span>Setting</span>
          </a>
        </li>
        <li class="my-4">
          <hr class="dropdown-divider">
        </li>
        <li>
          <div class="text-muted small fw-bold text-uppercase px-3">
            INTERFACE
          </div>
        </li>
        <li>
          <a class="nav-link px-3 sidebar-link" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            <span class="me-2"><i class="bi bi-basket-fill"></i></i></span>
            <span> Olah produk</span>
            <span class="rigth-icon ms-auto">
              <i class="bi bi-chevron-down"></i>
            </span>
          </a>
          <div class="collapse" id="collapseExample">
            <div>
              <ul class="navbar-nav ps-3">
                <li><a href="barang.php" class="nav-link px-3"><i class="bi bi-cart4"></i> Produk</a></li>
                <li><a href="tambah_produk.php" class="nav-link px-3"><i class="bi bi-cart-plus-fill"></i> Tambah Produk</a></li>
              </ul>
            </div>
          </div>
        </li>
        <li>
          <a href="feedback.php" class="nav-link px-3">
            <span class="me-2">
            </span>
            <span><i class="bi bi-envelope"></i> Feedback</span>
          </a>
        </li>
        <li>
          <a href="akun_user.php" class="nav-link px-3">
            <span class="me-2">
            </span>
            <span><i class="bi bi-people-fill"></i> Akun User</span>
          </a>
        </li>
        <li>
          <a href="daftar_admind.php" class="nav-link px-3">
            <span class="me-2">
            </span>
            <span><i class="bi bi-person-fill-add"></i> Daftar Admin Baru</span>
          </a>
        </li>
        <li>
          <a href="logout.php" class="nav-link px-3">
            <span class="me-2">
            </span>
            <span><i class="bi bi-box-arrow-right"></i> LogOut</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</div>



<!-- offcanvas -->