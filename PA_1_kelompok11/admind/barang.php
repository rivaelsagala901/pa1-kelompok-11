<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
    include("dashboard.php");
    ?>
    <!-- offcanvas -->
    <main class="mt-5 pt-3">
        <div class="container-fluid">
            <div class="row">

                <body>
                    <div class="container">
                        <h2 class="col-12 fw-bold fs-3">Tabel Produk</h2>
                        <input id="searchInput" class="form-control me-2 mt-5" type="search" name="search" placeholder="Search" aria-label="Search" style="width:20rem; border-color: black;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Harga Produk</th>
                                    <th>Deskripsi Produk</th>
                                    <th>Gambar Produk</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // Koneksi ke database
                                require "../config.php";
                                $koneksi = mysqli_connect($servername, $username, $password, $db_name);
                                if (mysqli_connect_errno()) {
                                    echo "Koneksi database gagal: " . mysqli_connect_error();
                                }
                                // Query untuk mengambil data produk
                                $query = "SELECT * FROM produk ORDER BY id_produk DESC";
                                $result = mysqli_query($koneksi, $query);
                                $nomor = 1; // Inisialisasi nomor urut
                                // Loop untuk menampilkan data produk dalam tabel
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<tr>";
                                    echo "<td>" . $nomor . "</td>"; // Tampilkan nomor urut
                                    echo "<td>" . $row['nama_produk'] . "</td>";
                                    echo "<td>Rp " . number_format($row['harga_produk'], 0, ',', '.') . "</td>";
                                    echo "<td>" . $row['deskripsi_produk'] . "</td>";
                                    echo "<td><img src='" . $row['gambar_produk'] . "' width='100'></td>";
                                    echo "<td><a href='edit_produk.php?id=" . $row['id_produk'] . "' class='btn btn-primary  bi bi-pen'>Edit</a> <a href='hapus_produk.php?id=" . $row['id_produk'] . "' class='btn btn-danger bi bi-trash3-fill'>Delete</a></td>";
                                    echo "</tr>";
                                    $nomor++; // Inkrementasi nomor urut
                                }
                                // Tutup koneksi ke database
                                mysqli_close($koneksi);
                                ?>


                            </tbody>
                        </table>
                    </div>
                </body>
            </div>
        </div>
    </main>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>


<script>
    $(document).ready(function() {
        // Mendeteksi perubahan pada input pencarian
        $('#searchInput').on('input', function() {
            var searchValue = $(this).val(); // Mendapatkan nilai pencarian

            // Mengirim permintaan AJAX ke server untuk melakukan pencarian
            $.ajax({
                url: 'search_barang.php', // Ganti dengan nama file PHP yang akan memproses pencarian
                method: 'GET',
                data: {
                    search: searchValue
                }, // Mengirim nilai pencarian ke server
                success: function(response) {
                    // Mengganti isi tabel dengan hasil pencarian yang diterima dari server
                    $('tbody').html(response);
                }
            });
        });
    });
</script>