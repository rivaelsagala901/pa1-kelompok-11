<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
    include("dashboard.php");
    ?>
    <!-- offcanvas -->
    <main class="mt-5 pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 fw-bold fs-3 text-center">Dashboard</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Akun User</h2>
                    <div class="container py-5">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title mb-0 text-center">Akun Sekka Craft</h1>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>User Name</th>
                                                <th>Password</th>
                                                <th>Account Created</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // Koneksi ke database
                                            include '../config.php';

                                            // Query untuk mengambil data dari tabel feedback
                                            $sql = "SELECT id_akun, username, password,last_login FROM akun WHERE roll='user'";

                                            // Eksekusi query
                                            $result = mysqli_query($conn, $sql);

                                            // Loop untuk menampilkan data ke dalam tabel
                                            while ($row = mysqli_fetch_assoc($result)) {
                                                echo "<tr>";
                                                echo "<td>" . $row['username'] . "</td>";
                                                echo "<td>" . $row['password'] . "</td>";
                                                echo "<td>" . $row['last_login'] . "</td>";
                                                echo "<td><a href='hapus_akun.php?id=" . $row['id_akun'] . "' class='btn btn-danger bi bi-trash3-fill'>Delete Akun</a></td>";

                                                echo "</tr>";
                                            }
                                            // Tutup koneksi ke database
                                            mysqli_close($conn);
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>