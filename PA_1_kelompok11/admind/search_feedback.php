<?php
// Koneksi ke database
include '../config.php';

// Cek apakah parameter pencarian 'search' telah dikirim melalui permintaan AJAX
if (isset($_GET['search'])) {
  // Dapatkan nilai pencarian
  $search = $_GET['search'];

  // Modifikasi query untuk mencari data berdasarkan kata kunci
  $sql = "SELECT id_feedback, nama, email, komentar FROM feedback WHERE nama LIKE '%$search%'";

  // Eksekusi query
  $result = mysqli_query($conn, $sql);

  $nomor = 1;
  // Loop untuk menampilkan data ke dalam tabel
  while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr>";
    echo "<td>" . $nomor . "</td>";
    echo "<td>" . $row['nama'] . "</td>";
    echo "<td>" . $row['email'] . "</td>";
    echo "<td class='text-wrap '>" . $row['komentar'] . "</td>";
    echo "<td><a href='hapus_feedback.php?id=" . $row['id_feedback'] . "' class='btn btn-danger bi bi-trash3-fill'>Delete</a></td>";
    echo "</tr>";
    $nomor++;
  }

  // Tutup koneksi ke database
  mysqli_close($conn);
}
