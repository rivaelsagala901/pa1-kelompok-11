<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <?php
  include("dashboard.php");
  ?>
  <main class="mt-5 pt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 fw-bold fs-3">Tambah Produk</div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <form action="tambah_produk_proses.php" method="POST" enctype="multipart/form-data">
            <div class="mb-3">
              <label for="nama_produk" class="form-label">Nama Produk</label>
              <input type="text" class="form-control" id="nama_produk" name="nama_produk" required>
            </div>
            <div class="mb-3">
              <label for="harga_produk" class="form-label">Harga Produk</label>
              <input type="number" class="form-control" id="harga_produk" name="harga_produk" required>
            </div>
            <div class="mb-3">
              <label for="deskripsi_produk" class="form-label">Deskripsi Produk</label>
              <textarea class="form-control" id="deskripsi_produk" name="deskripsi_produk" rows="3" required></textarea>
            </div>
            <div class="mb-3">
              <label for="gambar_produk" class="form-label">Gambar Produk</label>
              <input type="file" class="form-control" id="gambar_produk" name="gambar_produk" required>
            </div>
            <button type="submit" class="btn btn-primary">Tambah Produk</button>
          </form>
        </div>
      </div>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>