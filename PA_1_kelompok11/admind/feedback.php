<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="style.css">
  <style>
    .table-comment {
      table-layout: fixed;
    }

    .table-comment td {
      word-wrap: break-word;
      max-width: 200px;
      /* Ubah nilai sesuai kebutuhan Anda */
    }
  </style>
</head>

<body>
  <?php
  include("dashboard.php");
  ?>
  <!-- offcanvas -->
  <main class="mt-5 pt-3">
    <div class="container">
      <div class="row">
        <div class="col-12 fw-bold fs-3">Feedback</div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="container">

            <input id="searchInput" class="form-control me-2 mt-5" type="search" name="search" placeholder="Search" aria-label="Search" style="width:20rem;">

            <div class="container py-5">
              <div class="card">
                <div class="card-header">
                  <h1 class="card-title mb-0 text-center">Feedback Sekka Craft</h1>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Nomor</th>
                          <th>Nama</th>
                          <th>Email</th>
                          <th>Komentar</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        // Koneksi ke database
                        include '../config.php';

                        // Query untuk mengambil data dari tabel feedback
                        $sql = "SELECT id_feedback, nama, email, komentar,aksi FROM feedback ORDER BY id_feedback DESC";
                        // Eksekusi query
                        $result = mysqli_query($conn, $sql);
                        $nomor = 1;
                        // Loop untuk menampilkan data ke dalam tabel
                        while ($row = mysqli_fetch_assoc($result)) {
                          echo "<tr>";
                          echo "<td>" . $nomor. "</td>";
                          echo "<td>" . $row['nama'] . "</td>";
                          echo "<td>" . $row['email'] . "</td>";
                          echo "<td class='text-wrap '>" . $row['komentar'] . "</td>";
                          if ($row['aksi'] == 'tampil') {
                            echo "<td><button class='btn btn-info' disabled>Tampilkan</button></td>";
                            echo "<td><a href='feedback_aksi.php?id=" . $row['id_feedback'] . "&aksi=sembunyikan' class='btn btn-danger'>Sembunyikan</a></td>";
                          } else {
                            echo "<td><a href='feedback_aksi.php?id=" . $row['id_feedback'] . "&aksi=tampil' class='btn btn-info'>Tampilkan</a></td>";
                            echo "<td><button class='btn btn-danger' disabled>Sembunyikan</button></td>";
                          }
                              echo "</tr>";
                          $nomor++;
                        }

                        // Tutup koneksi ke database
                        mysqli_close($conn);
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>

<script>
  $(document).ready(function() {
    // Mendeteksi perubahan pada input pencarian
    $('#searchInput').on('input', function() {
      var searchValue = $(this).val(); // Mendapatkan nilai pencarian

      // Mengirim permintaan AJAX ke server untuk melakukan pencarian
      $.ajax({
        url: 'search_feedback.php', // Ganti dengan nama file PHP yang akan memproses pencarian
        method: 'GET',
        data: {
          search: searchValue
        }, // Mengirim nilai pencarian ke server
        success: function(response) {
          // Mengganti isi tabel dengan hasil pencarian yang diterima dari server
          $('tbody').html(response);
        }
      });
    });
  });
</script>