<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>

<body>
    <?php
    // Include koneksi ke database
    require '../config.php';

    // Memeriksa apakah tombol submit telah ditekan
    if (isset($_POST['submit'])) {
        // Mengambil nilai-nilai dari formulir
        $jamKerja = $_POST['jam_kerja'];
        $alamatToko = $_POST['alamat_toko'];
        $whatsappOwner = $_POST['whatsapp'];
        $whatsappPesan = $_POST['whatsapp_pesan'];
        $facebookOwner = $_POST['facebook'];
        $facebookToko = $_POST['facebook_toko'];
        $instagramOwner = $_POST['instagram'];
        $instagramToko = $_POST['instagram_toko'];
        $googlemaps = $_POST['google_maps'];

        // Query update data setting
        $query = "UPDATE setting SET 
                jam_kerja = '$jamKerja',
                alamat_toko = '$alamatToko',
                whatsapp = '$whatsappOwner',
                whatsapp_pesan = '$whatsappPesan',
                facebook = '$facebookOwner',
                facebook_toko = '$facebookToko',
                instagram = '$instagramOwner',
                instagram_toko = '$instagramToko',
                google_maps = '$googlemaps'
                WHERE id = 1";

        // Eksekusi query update data setting
        $result = mysqli_query($conn, $query);

        if ($result) { ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Data berhasil di Edit!',
                    onClose: function() {
                        window.location.href = "setting.php";
                    }
                });
            </script>
    <?php exit();
        } else {
            // Penanganan jika terjadi kesalahan pada query
            echo "Error: " . mysqli_error($conn);
        }
    }

    // Tutup koneksi ke database
    mysqli_close($conn);

    ?>

</body>

</html>