<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <?php
    include("dashboard.php");
    require "../config.php";



    // Query untuk mengambil data setting yang akan diubah
    $query = "SELECT * FROM setting WHERE id=1";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result);
    ?>



    <!-- offcanvas -->
    <main class="mt-5 pt-3">
        <div class="container-fluid">
            <h2 class="fw-bold fs-3">Setting</h2>

            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="setting_proses.php" method="post">
                        <div class="mb-3">
                            <label for="working-hours" class="form-label">Jam kerja</label>
                            <input type="text" class="form-control" id="jam_kerja" name="jam_kerja" value="<?php echo $row['jam_kerja']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="store-address" class="form-label">Alamat Toko</label>
                            <input type="text" class="form-control" id="alamat_toko" name="alamat_toko" value="<?php echo $row['alamat_toko']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="whatsapp" class="form-label">Link WhatsApp Owner</label>
                            <input type="text" class="form-control" id="whatsapp" name="whatsapp" value="<?php echo $row['whatsapp']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="phone-number" class="form-label">Link WhatsApp Untuk Pemesanan</label>
                            <input type="text" class="form-control" id="phone-number" name="whatsapp_pesan" value="<?php echo $row['whatsapp_pesan']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="facebook" class="form-label">Link Facebook Owner</label>
                            <input type="text" class="form-control" id="facebook" name="facebook" value="<?php echo $row['facebook']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="facebook-toko" class="form-label">Link Facebook Toko</label>
                            <input type="text" class="form-control" id="facebook-toko" name="facebook_toko" value="<?php echo $row['facebook_toko']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="instagram" class="form-label">Link Instagram Owner</label>
                            <input type="text" class="form-control" id="instagram" name="instagram" value="<?php echo $row['instagram']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="instagram-toko" class="form-label">Link Instagram Toko</label>
                            <input type="text" class="form-control" id="instagram-toko" name="instagram_toko" value="<?php echo $row['instagram_toko']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="google-maps" class="form-label">Link Google Maps Toko</label>
                            <input type="text" class="form-control" id="google-maps" name="google_maps" value="<?php echo $row['google_maps']; ?>">
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit">Save</button>
                    </form>
                </div>
            </div>

        </div>
    </main>

    <?php if (isset($_SESSION['teredit'])) : ?>
        <div class="edit-data" data-editdata="<?php echo $_SESSION['teredit']; ?>"></div>
    <?php unset($_SESSION['teredit']);
    endif; ?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


</body>

</html>
