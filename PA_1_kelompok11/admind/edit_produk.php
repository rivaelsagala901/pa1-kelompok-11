<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <?php
  include("dashboard.php");
  include("ambil_produk.php");
  ?>
  <!-- offcanvas -->
  <main class="mt-5 pt-3">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 fw-bold fs-3">Dashboard</div>
      </div>
      <div class="row">
        <div class="container">
          <h2>Tabel Edit Produk</h2>
          <div class="col-md-6">
            <form action="edit_produk_proses.php?id_produk=<?php echo $id_produk; ?>" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="nama_produk">Nama Produk</label>
                <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?php echo $row['nama_produk']; ?>" />
              </div>
              <div class="form-group">
                <label for="harga_produk">Harga Produk</label>
                <input type="text" class="form-control" id="harga_produk" name="harga_produk" value="<?php echo $row['harga_produk']; ?>" />
              </div>
              <div class="form-group">
                <label for="deskripsi_produk">Deskripsi Produk</label>
                <textarea class="form-control" id="deskripsi_produk" name="deskripsi_produk" rows="3"><?php echo $row['deskripsi_produk']; ?></textarea>
              </div>
              <div class="form-group">
                <label for="kategori">Tampilan</label><br>
                <label class="radio-inline">
                  <input type="radio" name="kategori" value="bestseller" <?php if ($row['kategori'] == "bestseller") echo "checked"; ?>> Tampilkan di Best Seller
                </label><br>
                <label class="radio-inline">
                  <input type="radio" name="kategori" value="jual"> Hapus dari Best Seller
                </label>
              </div>
              <div class="form-group">
                <label for="gambar_produk">Gambar</label>
                <?php if (!empty($row['gambar_produk'])) : ?>
                  <img src="<?php echo $row['gambar_produk']; ?>" alt="" width="100" />
                <?php endif; ?>
                <input type="file" class="form-control" id="gambar_produk" name="gambar_produk" />
              </div>
              <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
            </form>
          </div>
        </div>
      </div>

  </main>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>