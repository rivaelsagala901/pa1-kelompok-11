<?php
// Menghubungkan ke database
include("../config.php");

// Memeriksa apakah parameter id dan aksi telah diterima
if(isset($_GET['id']) && isset($_GET['aksi'])) {
    // Mendapatkan nilai id dan aksi dari parameter
    $id_feedback = $_GET['id'];
    $aksi = $_GET['aksi'];

    // Memperbarui data feedback sesuai dengan aksi yang dipilih
    $query = "UPDATE feedback SET aksi = '$aksi' WHERE id_feedback = $id_feedback";
    $result = mysqli_query($conn, $query);

    if($result) {
        // Jika pembaruan berhasil, kembali ke halaman sebelumnya
        header("Location: feedback.php");
        exit;
    } else {
        echo "Error: " . mysqli_error($conn);
    }
}


// Menutup koneksi ke database
mysqli_close($conn);
