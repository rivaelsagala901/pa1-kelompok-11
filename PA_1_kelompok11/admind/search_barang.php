<?php
// Koneksi ke database
include '../config.php';

// Cek apakah parameter pencarian 'search' telah dikirim melalui permintaan AJAX
if (isset($_GET['search'])) {
    // Dapatkan nilai pencarian
    $search = $_GET['search'];

    // Modifikasi query untuk mencari data berdasarkan kata kunci
    $sql = "SELECT * FROM produk WHERE nama_produk LIKE '%$search%'";

    // Eksekusi query
    $result = mysqli_query($conn, $sql);

    $nomor = 1; // Inisialisasi nomor urut
    // Loop untuk menampilkan data produk dalam tabel
    while ($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $nomor . "</td>"; // Tampilkan nomor urut
        echo "<td>" . $row['nama_produk'] . "</td>";
        echo "<td>Rp " . number_format($row['harga_produk'], 0, ',', '.') . "</td>";
        echo "<td>" . $row['deskripsi_produk'] . "</td>";
        echo "<td><img src='" . $row['gambar_produk'] . "' width='100'></td>";
        echo "<td><a href='edit_produk.php?id=" . $row['id_produk'] . "' class='btn btn-primary  bi bi-pen'>Edit</a> <a href='hapus_produk.php?id=" . $row['id_produk'] . "' class='btn btn-danger bi bi-trash3-fill'>Delete</a></td>";
        echo "</tr>";
        $nomor++; // Inkrementasi nomor urut
    }
    // Tutup koneksi ke database
    mysqli_close($koneksi);
}