<?php
// Include koneksi ke database
require '../config.php';

// Memeriksa apakah tombol submit telah ditekan
if (isset($_POST['submit'])) {
  // Mengambil ID produk dari parameter URL
  $id_produk = $_GET['id_produk'];

  // Mendapatkan nilai-nilai dari formulir
  $nama_produk = $_POST['nama_produk'];
  $harga_produk = $_POST['harga_produk'];
  $deskripsi_produk = $_POST['deskripsi_produk'];
  $kategori = $_POST['kategori'];

  // Mendapatkan gambar lama dari database
  $query_get_gambar = "SELECT gambar_produk FROM produk WHERE id_produk = '$id_produk'";
  $result_get_gambar = mysqli_query($conn, $query_get_gambar);
  $row_get_gambar = mysqli_fetch_assoc($result_get_gambar);
  $gambar_produk_lama = $row_get_gambar['gambar_produk'];

  // Memeriksa apakah file gambar telah diunggah
  if (isset($_FILES['gambar_produk']) && $_FILES['gambar_produk']['error'] !== 4) {
    $gambar_produk = $_FILES['gambar_produk']['name'];
    $gambar_tmp = $_FILES['gambar_produk']['tmp_name'];

    // Memindahkan gambar ke direktori tujuan
    move_uploaded_file($gambar_tmp, "uploads/" . $gambar_produk);

    // Query update data produk beserta gambar
    $query = "UPDATE produk SET 
                nama_produk = '$nama_produk',
                harga_produk = '$harga_produk',
                deskripsi_produk = '$deskripsi_produk',
                gambar_produk = 'uploads/$gambar_produk',
                kategori = '$kategori'
                WHERE id_produk = '$id_produk'";
  } else {
    // Jika tidak ada gambar baru diunggah, tetap gunakan gambar lama
    $query = "UPDATE produk SET 
                nama_produk = '$nama_produk',
                harga_produk = '$harga_produk',
                deskripsi_produk = '$deskripsi_produk',
                kategori = '$kategori'
                WHERE id_produk = '$id_produk'";
  }

  // Eksekusi query update data produk
  $result = mysqli_query($conn, $query);

  if ($result) {
    // Redirect kembali ke halaman barang.php setelah proses selesai
    header("Location: barang.php");
    exit();
  } else {
    // Penanganan jika terjadi kesalahan pada query
    echo "Error: " . mysqli_error($conn);
  }
}

// Tutup koneksi ke database
mysqli_close($conn);
