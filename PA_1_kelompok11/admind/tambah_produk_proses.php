<?php
// menghubungkan ke database
include("../config.php");

// mengambil data dari form
$nama_produk = $_POST['nama_produk'];
$harga_produk = $_POST['harga_produk'];
$deskripsi_produk = $_POST['deskripsi_produk'];

// mengambil data gambar
$gambar_produk = $_FILES['gambar_produk']['name'];
$tmp_gambar_produk = $_FILES['gambar_produk']['tmp_name'];
$gambar_produk_path ="uploads/" . $gambar_produk;

// mengecek tipe file gambar yang diupload
$allowed_extension = array("jpg", "jpeg", "png");
$file_extension = pathinfo($gambar_produk_path, PATHINFO_EXTENSION);


// Validasi nama_produk
$query = "SELECT * FROM produk WHERE nama_produk = '$nama_produk'";
$result = mysqli_query($conn, $query);

if (mysqli_num_rows($result) > 0) {
    echo "<script>alert('Nama produk sudah ada'); window.location.href = 'tambah_produk.php';</script>";
    // Tambahkan tindakan lain yang sesuai dengan kebutuhan Anda, seperti menghentikan penyimpanan data atau menampilkan pesan kesalahan kepada pengguna.
} else {
    // Validasi harga_produk
    if ($harga_produk < 0 || $harga_produk === '-') {
        echo "<script>alert('Harga produk tidak valid'); window.location.href = 'tambah_produk.php';</script>";
        // Tambahkan tindakan lain yang sesuai dengan kebutuhan Anda, seperti menghentikan penyimpanan data atau menampilkan pesan kesalahan kepada pengguna.
    } else {
        if (!in_array($file_extension, $allowed_extension)) {
            echo "<script>alert('Format gambar tidak diizinkan (harus .jpg, .png, atau .jpeg)'); window.location.href = 'tambah_produk.php';</script>";
        } else {
            // Cek apakah gambar sudah ada di folder uploads
            if (file_exists($gambar_produk_path)) {
                echo "<script>alert('Gambar produk sudah ada'); window.location.href = 'tambah_produk.php';</script>";
                // Tambahkan tindakan lain yang sesuai dengan kebutuhan Anda, seperti menghentikan penyimpanan data atau menampilkan pesan kesalahan kepada pengguna.
            } else {
                // upload gambar ke folder uploads
                if (move_uploaded_file($tmp_gambar_produk, $gambar_produk_path)) {
                    // memasukkan data produk ke dalam tabel produk pada database
                    $query = "INSERT INTO produk (nama_produk, harga_produk, deskripsi_produk, gambar_produk) VALUES ('$nama_produk', '$harga_produk', '$deskripsi_produk', '$gambar_produk_path')";

                    $result = mysqli_query($conn, $query);

                    // cek apakah data produk berhasil ditambahkan ke dalam tabel pada database
                    if ($result) {
                        echo "<script>alert('Data produk berhasil ditambahkan ke dalam database.'); window.location.href = 'tambah_produk.php';</script>";
                    } else {
                        echo "Error: " . $query . "<br>" . mysqli_error($conn);
                    }
                } else {
                    echo "<script>alert('Gagal mengupload gambar'); window.location.href = 'tambah_produk.php';</script>";
                }
            }
        }
    }
}




// menutup koneksi ke database
mysqli_close($conn);
