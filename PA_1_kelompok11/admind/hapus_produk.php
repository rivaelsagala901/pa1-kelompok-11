<?php
require "../config.php";

// Koneksi ke database
$koneksi = mysqli_connect($servername, $username, $password, $db_name);

// Mengambil id_produk yang akan dihapus
$id_produk = $_GET['id'];

// Query untuk menghapus data produk
$query = "SELECT * FROM produk WHERE id_produk = '$id_produk'";
$result = mysqli_query($koneksi, $query);

if(mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $gambar_produk_path = $row['gambar_produk'];

    // Menghapus data produk dari database
    $delete_query = "DELETE FROM produk WHERE id_produk = '$id_produk'";
    $delete_result = mysqli_query($koneksi, $delete_query);

    if($delete_result) {
        // Menghapus file foto dari folder "uploads"
        if(file_exists($gambar_produk_path)) {
            unlink($gambar_produk_path);
        }
        // Redirect ke halaman utama
        header("Location: barang.php");
    } else {
        echo "Gagal menghapus produk";
    }
} else {
    echo "Data produk tidak ditemukan";
}

// menutup koneksi ke database
mysqli_close($koneksi);
