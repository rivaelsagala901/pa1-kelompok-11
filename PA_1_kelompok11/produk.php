    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>produk</title>
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
        <link rel="stylesheet" href="css/footerr.css">
        <link rel="stylesheet" href="css/card.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

        <style>
            .card-img-top {
                width: 100%;
                height: 100%;
                /* mengatur tinggi gambar */
                object-fit: cover;
                /* memastikan gambar tidak terpotong dan menyesuaikan ukuran */
            }

            body {
                padding-top: 70px;
            }

            @media (max-width: 992px) {
                body {
                    padding-top: 0;
                }
            }
        </style>

    </head>

    <body>
        <?php
        include("navbar.php");
        ?>
        <section class="tarik mt-5 pt-4">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-md-6" data-aos="fade-down" data-aos-duration="500" data-aos-delay="200">
                        <h1 class="display-4 fw-bold text-center">Selamat Datang di Halaman Produk Kami</h1>
                        <hr>
                    </div>
                    <div class="col-md-6" data-aos="fade-up">
                        <img src="gambar/1.png" alt="Banner Sekka Craft" class="img-fluid">
                    </div>
                </div>
            </div>

        </section>


        <header id="header" class="bg-dark py-5" style="margin-top: 50px; margin-bottom: 50px;" data-aos="fade-down">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder">Produk Kami</h1>
                    <p class="lead fw-normal text-white-50 mb-0">anda dapat membeli produk yang tersedia</p>
                </div>
            </div>
        </header>
        <input id="searchInput" type="text" placeholder="Cari produk..." onkeyup="searchProducts()" />
        <div class="container-fluid p-5">
            <div class="row" id="productList">
                <!-- Daftar produk akan ditampilkan di sini -->
            </div>
        </div>


        <div class="container-fluid p-5">
            <div class="row">
                <?php
                // Koneksi ke database
                require "config.php";
                // Query untuk mengambil data produk
                $query = "SELECT * FROM produk WHERE kategori IN ('jual', 'bestseller')";
                $result = mysqli_query($conn, $query);

                // Periksa apakah ada produk yang ditemukan
                if (mysqli_num_rows($result) > 0) {
                    // Loop melalui setiap produk dan menampilkannya
                    while ($row = mysqli_fetch_assoc($result)) {
                ?>
                        <div class="col-md-5 mb-4 border  mx-auto" data-aos="fade-up" style="display: flex; flex-wrap: wrap; margin-right: 10px; background-color: #ffffff; border-radius: 8px; box-shadow: 0 4px 6px rgba(0.8, 0.8, 0.8, 0.8);">
                            <div class="col-md-6" style="align-items: center;">
                                <img src="admind/<?php echo $row['gambar_produk']; ?>" class="produk-img" alt="...">
                            </div>
                            <div class="px-5 pb-5 col-md-6">
                                <h3 class="font-semibold tracking-tight text-gray-900 dark:text-white"><?php echo $row['nama_produk'] ?></h3>
                                <h6 class="font-semibold tracking-tight text-gray-900 dark:text-white"><?php echo $row['deskripsi_produk'] ?></h6>
                                <div class="flex items-center justify-between">
                                    <span class="text-3xl font-bold text-gray-900 dark:text-white">Rp.<?php echo number_format($row['harga_produk'], 0, ',', '.') ?></span>
                                </div>
                                <div class="mt-4">
                                    <a href="detail_produk.php?id=<?php echo $row['id_produk']; ?>" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Beli</a>
                                </div>
                            </div>
                        </div>



                <?php
                    }
                }

                // Tutup koneksi ke database
                mysqli_close($conn);
                ?>
            </div>
        </div>

        <!-- footer -->
        <?php
        include 'footer.php';
        ?>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init();
        </script>
        <script src="js/home.js"></script>
        <script src="js/script.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
        <!-- ... -->
    </body>

    </body>

    </html>