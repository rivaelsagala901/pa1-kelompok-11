<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <title>Daftar</title>
  <style>
    .container {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }

    .card {
      max-width: 500px;
      width: 90%;
      border-radius: 45px;
    }

    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: 50vh;
    }
  </style>
</head>

<body class="header finisher-header">
  <div class="container" data-aos="flip-left">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <img src="uploads/logo sekka2.jpg" class="img-fluid" alt="..." width="200px">
          <br>
          <h3><b>Daftar</b></h3>
          <hr>
          <form action="register_process.php" method="post">
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Username</label>
              <input type="text" class="form-control rounded-pill" id="exampleFormControlInput1" name="username" style="max-width: 300px;">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput2" class="form-label">Password</label>
              <input type="password" class="form-control rounded-pill" id="exampleFormControlInput2" name="password" required style="max-width: 300px;">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput3" class="form-label">Konfirmasi Password</label>
              <input type="password" class="form-control rounded-pill" id="exampleFormControlInput3" name="password_confirmation" required style="max-width: 300px;">
            </div>
            <br>
            <button class="btn btn-primary rounded-pill" name="submit">Daftar</button>
            <span class="d-block d-md-inline">Sudah Memiliki Akun? | <a href="login.php" class="text-decoration-none">Masuk Disini</a></span>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <script src="js/finisher-header.es5.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    new FinisherHeader({
      "count": 10,
      "size": {
        "min": 1300,
        "max": 1500,
        "pulse": 0
      },
      "speed": {
        "x": {
          "min": 0.1,
          "max": 0.6
        },
        "y": {
          "min": 0.1,
          "max": 0.6
        }
      },
      "colors": {
        "background": "#30b718",
        "particles": [
          "#2de423",
          "#000000",
          "#2235e5",
          "#000000",
          "#161515"
        ]
      },
      "blending": "overlay",
      "opacity": {
        "center": 0.5,
        "edge": 0.05
      },
      "skew": 0,
      "shapes": [
        "c"
      ]
    });
  </script>
</body>

</html>