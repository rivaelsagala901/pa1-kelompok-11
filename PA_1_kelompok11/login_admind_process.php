<?php
require "config.php";

$username = mysqli_real_escape_string($conn, $_POST["username"]); // menghindari SQL injection
$password = mysqli_real_escape_string($conn, $_POST["password"]); // menghindari SQL injection

$query_sql = "SELECT * FROM admind WHERE username = '$username' AND password = '$password'";
$result = mysqli_query($conn, $query_sql);

if (mysqli_num_rows($result) > 0) {
    $row_akun = mysqli_fetch_array($result);
    session_start();
    $_SESSION["akun_id"] = $row_akun["id_akun"];
    $_SESSION['username'] = $username;
    $_SESSION['password'] = $password;
    header("Location:admind");
    exit(); // keluar dari script setelah header() dijalankan
} else {
    echo '<script>alert("Username atau password salah!"); window.location.href = "login_admind.php";</script>';
    exit(); // keluar dari script setelah alert() dijalankan
}
?>